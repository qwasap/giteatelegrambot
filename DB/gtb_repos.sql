CREATE TABLE `gtb_repos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tlg_user_id` bigint(20) NOT NULL,
  `repo_url` varchar(400) NOT NULL,
  `repo_sha256` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
