Gitea Telegram Bot
==================

To install the bot just follow the next steps:

1) Download the code to your server.

2) Download all extra code with composer. You can check it from composer.json.

3) Update **/application/config/config.php** with your base_url and your Telegram_BOT_TOKEN

4) Update **/application/config/database.php** with you database connection data

5) Import into your database de table structures found in DB folder

6) Configure your bot through Telegram to point to the Payload.php file

That's it.


Inspiration
===========
Inspired by [cubotto.org](http://cubotto.org/) and built with its crow support to share it with everybody else.
