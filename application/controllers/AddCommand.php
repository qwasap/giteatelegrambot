<?php

namespace Vendor\App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class AddCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "add";

    /**
     * @var string Command Description
     */
    protected $description = "Add a new secret phrase to identify the repo that you want to listen";

    /**
     * @var object CodeIgniter Base
     */
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        log_message('debug', "[Add command] ".print_r($this->getUpdate()->recentMessage(),true));

    	$params = explode(" ", $arguments);				
    	$params = array_map('trim',$params);
    	$params = array_values(array_filter($params)); // remove empty items

    	$input_data = array(
    		'url' => array_key_exists(0, $params) ? $params[0] : null,
    		'secret' => array_key_exists(1, $params) ? $params[1] : null,
    	);

    	$this->CI->load->library('form_validation');
		$this->CI->form_validation->set_data($input_data);

		$this->CI->form_validation->set_rules('url', 'Repo url', 'trim|required|min_length[5]|max_length[500]|valid_url');
		$this->CI->form_validation->set_rules('secret', 'Secret', 'trim|required|min_length[5]|max_length[500]');

		if ($this->CI->form_validation->run() == FALSE)
		{
		    $res = validation_errors('⚠️ ',' ');
		    $res .= PHP_EOL . '❔ Form of use: ';
		    $res .= PHP_EOL . "/add repo_url webhook_secret";
		}
		else
		{			
			// $user_id = $this->getUpdate()->recentMessage()->getFrom()->getId();
            $chat_id = $this->getUpdate()->recentMessage()->getChat()->getId();

			$this->CI->load->model('repos_model', '', true);
			$action_result = $this->CI->repos_model->add_new_repo_listener($chat_id, $input_data['url'], $input_data['secret']);

			if ($action_result === true)
		    	$res = '✅ Listening updates from repo ' . $input_data['url'] . ' with secret ' . $input_data['secret'];
		    else
		    	$res = '❌ ' . $action_result;
		}

        $this->replyWithMessage(['text' => $res]);
    }
}

