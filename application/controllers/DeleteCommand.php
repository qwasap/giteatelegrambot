<?php

namespace Vendor\App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class DeleteCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "delete";

    /**
     * @var string Command Description
     */
    protected $description = "Remove a repo to stop receiving updates from it";

    /**
     * @var object CodeIgniter Base
     */
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        log_message('debug', "[Delete command] ".print_r($this->getUpdate()->recentMessage(),true));
    	
        $params = explode(" ", $arguments);				
    	$params = array_map('trim',$params);
    	$params = array_values(array_filter($params)); // remove empty items

    	$input_data = array(
    		'repourl' => array_key_exists(0, $params) ? $params[0] : null,
    	);

    	$this->CI->load->library('form_validation');
		$this->CI->form_validation->set_data($input_data);

		$this->CI->form_validation->set_rules('repourl', 'Repo url', 'trim|required|min_length[5]|max_length[500]|valid_url');

		if ($this->CI->form_validation->run() == FALSE)
		{
		    $text = validation_errors('⚠️ ',' ');
		    $text .= PHP_EOL . '❔ Form of use: ';
		    $text .= PHP_EOL . "/delete repo_url";

	    	$this->replyWithMessage([
	            'text' => $text,
	        ]);
		}
		else
		{			
			// $user_id = $this->getUpdate()->recentMessage()->getFrom()->getId();
            $chat_id = $this->getUpdate()->recentMessage()->getChat()->getId();

			$this->CI->load->model('repos_model', '', true);
			$action_result = $this->CI->repos_model->remove_listener_from_user($chat_id, $input_data['repourl']);

			if ($action_result === true)
			{
				$this->triggerCommand('list');
			}
		    else
		    {
		    	$this->replyWithMessage([
		            'text' => '❌ ' . $action_result,
		        ]);
		    }
		}
    }
}

