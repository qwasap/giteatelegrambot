<?php

namespace Vendor\App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class ListCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "list";

    /**
     * @var string Command Description
     */
    protected $description = "List all the repos that you have configured";

    /**
     * @var object CodeIgniter Base
     */
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        log_message('debug', "[List command] ".print_r($this->getUpdate()->recentMessage(),true));
        
        $this->CI->load->model('repos_model', '', true);

		// $user_id = $this->getUpdate()->recentMessage()->getFrom()->getId();
        $chat_id = $this->getUpdate()->recentMessage()->getChat()->getId();

        // $repo_list = $this->CI->repos_model->get_repos_by_user($user_id);
		$repo_list = $this->CI->repos_model->get_repos_by_user($chat_id);

		$keyboard = array();
		foreach ($repo_list as $value)
		{
			$keyboard[][] = "/delete " . $value->repo_url;
		}
        $keyboard[][] = "Close list";

		$reply_markup = $this->getTelegram()->replyKeyboardMarkup([
			'keyboard' => $keyboard, 
			'resize_keyboard' => true, 
			'one_time_keyboard' => true,
			'selective' => false

		]);

		$this->replyWithMessage([
        	'text' => "You have " . count($repo_list) . (count($repo_list) == 1 ? ' repository ' : ' repositories ') . "actived. Click on the button of any of them to delete it and to spot receiving updates.",
			'reply_markup' => $reply_markup
        ]);
    }
}

