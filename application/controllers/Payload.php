<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Telegram\Bot\Api;

class Payload extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();

    }

	public function index()
	{
		$stream = $this->input->raw_input_stream;
		
		if (!is_null($stream))
		{
			$data = json_decode($stream);
			if (!is_null($data))
			{
				log_message('debug', '[ formated data payload ] ' . print_r($data, true));

				$this->load->model('repos_model', '', true);
				$listeners = $this->repos_model->get_repo_listeners($data->repository->clone_url, $data->secret);

				if (count($listeners))
				{
					$tlg = new Api($this->config->item('Telegram_BOT_TOKEN'));
					
					foreach ($listeners as $listener) 
					{
						// default message to listeners
						$text = PHP_EOL . "*" . $data->sender->username . "* changed [".$data->repository->full_name."](".$data->repository->html_url.")";

						// message if commit
						if (isset($data->commits))
						{
							$text  = count($data->commits) . " new commit". (count($data->commits) > 1 ? 's' : '') ." found in [".$data->repository->full_name."](".$data->repository->html_url.")";

							foreach ($data->commits as $commit) 
							{
								$text .= PHP_EOL . "*" . $commit->committer->name . "* pushed at _" .$commit->timestamp. "_ with id [" . $commit->id . "](" . $commit->url . ")" . PHP_EOL . "➡️ " . $commit->message;
							}
						}

						// message if branch
						if (isset($data->ref_type) && $data->ref_type == 'branch')
						{
							$text = PHP_EOL . "New branch *" . $data->ref . "* created in [".$data->repository->full_name."](".$data->repository->html_url.") " . $data->repository->description;
						}

						// message if pull request
						if (isset($data->pull_request))
						{
							$text = PHP_EOL . "Pull request *".$data->pull_request->state."* by *" . $data->pull_request->user->username . "* in [" . $data->repository->full_name . "](" . $data->repository->html_url. ")" . PHP_EOL . "➡️ " . $data->pull_request->title;
						}

						// sending message to listener
						$params = [
							'chat_id' => $listener->tlg_user_id,
							'text' => $text,
							'parse_mode' => 'Markdown',
						]; 

						$tlg->sendMessage($params);		
					}
				}
			}
		}
	}

}
