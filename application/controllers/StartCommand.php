<?php

namespace Vendor\App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Learn how to use this bot";

    /**
     * @var object CodeIgniter Base
     */
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
    }

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        log_message('debug', "[Start command] ".print_r($this->getUpdate()->recentMessage(),true));

        $this->replyWithMessage(['text' => 'Hello! Welcome to Gitea bot. Here are our available commands:']);
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $commands = $this->getTelegram()->getCommands();

        $response = '';
        foreach ($commands as $name => $command) {
            $response .= sprintf('/%s - %s' . PHP_EOL, $name, $command->getDescription());
        }

        $this->replyWithMessage(['text' => $response]);

        $response = "To connect any Gitea repository with the bot, please, first head to your Gitea account to open de *Configuration* tab of that repository that you would like to link." . PHP_EOL . PHP_EOL . "Once there, click on the *Webhooks* option to create a new hook with de following data:" .PHP_EOL . PHP_EOL . "*Payload URL:* " . base_url('payload') . PHP_EOL . "*Content type:* application/json" . PHP_EOL . "*Secret:* chose any random string of your choice. The longer the better." . PHP_EOL . PHP_EOL . "Once you have got your secret you can add it to the bot with the command _/add repo-url hook-secret_. Use the main http url of the repo showed by Gitea and the secret that you previously created setting up the hook" . PHP_EOL . PHP_EOL . "From this moment on you will start to get the updates from the repository"; 

        $this->replyWithMessage([
            'text' => $response,
            'parse_mode' => 'Markdown',
        ]);
    }
}

