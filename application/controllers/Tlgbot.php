<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Telegram\Bot\Api;

include_once('StartCommand.php');
include_once('AddCommand.php');
include_once('ListCommand.php');
include_once('DeleteCommand.php');

class Tlgbot extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('url');
    }

	public function index()
	{
		$this->load->view('welcome_message');
	}

	// set webhook url to Telegram
	// visit gitea.bitmachine.org/index.php/tlgbot/setwebhook
	public function setwebhook()
	{
		$tlg = new Api($this->config->item('Telegram_BOT_TOKEN'));

		$webhook_url = site_url([
			'tlgbot',
			'webhook',
			$this->config->item('Telegram_BOT_TOKEN')
		]);

		$tlg->setWebhook(['url' => $webhook_url]);

		$this->load->view('welcome_message');
	}

	public function webhook($token = null)
	{
		if ($token == $this->config->item('Telegram_BOT_TOKEN')) {

			$tlg = new Api($this->config->item('Telegram_BOT_TOKEN'));
			
			// just to check if library works
			// $response = $tlg->getMe();
			// log_message('debug',print_r($response, true)); die();
	
			$tlg->addCommands([
				Telegram\Bot\Commands\HelpCommand::class,
				Vendor\App\Commands\StartCommand::class,
				Vendor\App\Commands\AddCommand::class,
				Vendor\App\Commands\ListCommand::class,
				Vendor\App\Commands\DeleteCommand::class,
			]);

			// $update = $tlg->getWebhookUpdates();
			$update = $tlg->commandsHandler(true);
			// log_message('debug', "[Webhook Update] ".print_r($update,true));

			$msg = $update->recentMessage();
			$msg_raw = $msg->getRawResponse();

			$msg_is_command = false;
			if (array_key_exists('entities', $msg_raw)) foreach($msg_raw['entities'] as $entity)
			{
				if ($entity['type'] == 'bot_command' && $entity['offset'] == 0) $msg_is_command = true;
			}

			if (!$msg_is_command)
			{
				if ($msg->getText() != 'Close list')
				{
					$chat_type = $msg->getChat()->getType();
					if ($chat_type == 'private')
					{
						$params = [
							'chat_id' => $msg->getChat()->getId(),
							'text' => 'I do not understand this: "' . $msg->getText() . '". Please, check /help to work with me.',
						]; 

						$tlg->sendMessage($params);				
					}
				}
				else
				{

					$reply_markup = $tlg->replyKeyboardHide();

					$params = [
						'chat_id' => $msg->getChat()->getId(),
						'text' => 'List closed',
						'reply_markup' => $reply_markup
					]; 

					$tlg->sendMessage($params);									
				}
			}
		}
	}

}
