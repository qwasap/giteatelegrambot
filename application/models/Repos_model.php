<?php

class Repos_model extends CI_Model 
{
    public function add_new_repo_listener($user_id, $repo_url, $repo_secret)
    {
    	$data = array(
    		'tlg_user_id' => $user_id,
    		'repo_url' => $this->clean_url($repo_url),
    		'repo_sha256' => $this->get_hash([$this->clean_url($repo_url), $repo_secret])
    	);

    	$this->db->where($data);
    	$query = $this->db->get('repos');

    	if (!$query->num_rows())
    	{
    		$this->db->insert('repos', $data);
    		return true;
    	}
    	else
    	{
    		return 'Repository listener already exists';
    	}
    }

    public function get_repo_listeners($repo_url, $repo_secret)
    {
    	$this->db->select('tlg_user_id');
    	$this->db->where([
    		'repo_url' => $this->clean_url($repo_url),
    		'repo_sha256' => $this->get_hash([$this->clean_url($repo_url), $repo_secret]),
    	]);

    	$query = $this->db->get('repos');
    	return $query->result();
    }

    public function get_repos_by_user($user_id)
    {
    	$this->db->select('id, repo_url');
    	$this->db->where('tlg_user_id', $user_id);
    	$query = $this->db->get('repos');

    	return $query->result();
    }

    public function remove_listener_from_user($user_id, $repo_url)
    {
    	$this->db->select('id');
    	$this->db->where('tlg_user_id', $user_id);
    	$this->db->where('repo_url', $repo_url);
    	$query = $this->db->get('repos');

    	if ($query->num_rows())
    	{
    		$row = $query->row();
    		$this->db->where('id', $row->id);
    		$this->db->delete('repos');

    		return true;
    	}
    	else
    	{
    		return "You are trying to remove a repo that is not in your list";
    	}
    }

    public function get_hash($text_list = array())
    {
    	return hash('sha256', implode('', $text_list));
    }

    public function clean_url($url)
    {
    	// remove protocol
    	$url = parse_url($url, PHP_URL_HOST) . parse_url($url, PHP_URL_PATH);

    	// remove extension
    	if (substr($url, -4) == '.git') $url = str_replace('.git', '/', $url);

    	// add final slash if missing
		if (substr($url, -1) != '/') $url .= '/';

    	return $url;
    }
}